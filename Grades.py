#!/usr/bin/env python3

# pylint: disable = invalid-name
# pylint: disable = missing-docstring
# pylint: disable = line-too-long

# ---------
# Grades.py
# ---------

# https://docs.python.org/3.6/library/functools.html
# https://sphinx-rtd-tutorial.readthedocs.io/en/latest/docstrings.html

# -----------
# grades_eval
# -----------


# get the letter grade given the total number of assignments completed
# per category (in the order of projects, exercises, blogs, papers, quizzes)
# the number of assignments completed per category can be uncapped (> max)
def get_grade(scores: list[int]) -> str:
    assert scores and len(scores) == 5

    # cap the grades to ensure that larger values cannot be viewed as F's
    cap: list[int] = [5, 11, 13, 13, 39]
    for i, score in enumerate(scores):
        if score > cap[i]:
            scores[i] = cap[i]

    # dicts representing mapping from numerical to letter grades
    # if not in dict, then F
    sp = {5: "A", 4: "B+", 3: "D+"}
    se = {11: "A", 10: "B+", 9: "C+", 8: "C-", 7: "D-"}
    sbr = {13: "A", 12: "B+", 11: "B-", 10: "C", 9: "D+", 8: "D-"}
    sq = {
        39: "A",
        38: "A-",
        37: "B+",
        36: "B",
        35: "B",
        34: "B-",
        33: "C+",
        32: "C+",
        31: "C",
        30: "C-",
        29: "C-",
        28: "D+",
        27: "D",
        26: "D-",
        25: "D-",
    }

    # dict representing the value of each grade to calc the lowest category
    # allows us to compare the letter grade values
    grade_val = {
        "A": 11,
        "A-": 10,
        "B+": 9,
        "B": 8,
        "B-": 7,
        "C+": 6,
        "C": 5,
        "C-": 4,
        "D+": 3,
        "D": 2,
        "D-": 1,
        "F": 0,
    }

    # find minimum grade
    min_grade = "A"
    cur_grade: str
    rubric: list[dict[int, str]] = [sp, se, sbr, sbr, sq]
    for i in range(0, 5):
        # calculate grade given the category
        cur_grade = rubric[i][scores[i]] if scores[i] in rubric[i] else "F"
        # update min_grade if cur_grade is lower
        if grade_val[cur_grade] < grade_val[min_grade]:
            min_grade = cur_grade

    assert min_grade
    return min_grade


# return the letter grade given a list[list[int]]
# representing the score of each assignment per category.
# determined by the lowest grade in all categories
def grades_eval(l_l_scores: list[list[int]]) -> str:
    assert l_l_scores
    scores: list[int] = [0, 0, 0, 0, 0]
    index: int = 0

    # loop through each line (category) in l_l_scores
    for line in l_l_scores:
        # keeps count of each score for the current category
        count: list[int] = [0, 0, 0, 0]
        for score in line:
            count[score] += 1
        scores[index] = count[2] + count[3] + min(count[3] // 2, count[1])
        index += 1

    assert scores
    return get_grade(scores)

# CS373: Software Engineering Grades Repo

* Name: Alexander Lee

* EID: al55332

* GitLab ID: alexlee8210

* HackerRank ID: alexlee8210

* Git SHA: b5939b1928be02681a4c256b07ae3d8c85578d19

* GitLab Pipelines: https://gitlab.com/alexlee8210/cs373-grades/-/pipelines/1156766993

* Estimated completion time: 15

* Actual completion time: 20

* Comments: None
